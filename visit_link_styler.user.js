// ==UserScript==
// @name         Visit link styler
// @namespace    https://javelin.works
// @version      0.3.1
// @description  訪問済みリンクのスタイルを変える
// @author       sanadan
// @match        *://*/*
// ==/UserScript==

(function () {
  'use strict'

  document.head.insertAdjacentHTML('beforeend', '<style>a:visited{color:green !important;}</style>')
})()
